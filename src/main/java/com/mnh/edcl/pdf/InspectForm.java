package com.mnh.edcl.pdf;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.List;
import java.util.Map;

import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.AcroFields;
import com.itextpdf.text.pdf.BaseField;
import com.itextpdf.text.pdf.PdfDictionary;
import com.itextpdf.text.pdf.PdfName;
import com.itextpdf.text.pdf.PdfReader;

public class InspectForm {

    /** A text file containing information about a form. */
    //public static final String RESULTTXT = "results/part4/chapter13/fieldflags.txt";

    /**
     * Inspects a PDF file src with the file dest as result
     * @param src the original PDF
     * @param dest the resulting PDF
     * @throws IOException
     * @throws DocumentException
     */
    public void inspectPdf(String src, String dest)
            throws IOException, DocumentException {
        OutputStreamWriter out = new OutputStreamWriter(new FileOutputStream(dest));
        PdfReader reader = new PdfReader(src);
        AcroFields form = reader.getAcroFields();
        Map<String,AcroFields.Item> fields = form.getFields();
        AcroFields.Item item;
        PdfDictionary dict;
        int flags;
        StringBuilder sb = new StringBuilder();
        String key;
        String jsonValue;

        for (Map.Entry<String,AcroFields.Item> entry : fields.entrySet()) {
            key = entry.getKey();
            //out.write(key);
            item = entry.getValue();
            dict = item.getMerged(0);

            switch (form.getFieldType(key)) {
                    case AcroFields.FIELD_TYPE_CHECKBOX:
                        jsonValue=" 'name': '"+key+"', 'type' : 'Checkbox'";
                        break;
                    case AcroFields.FIELD_TYPE_COMBO:
                        jsonValue=" 'name': '"+key+"','type' : 'Combobox'";
                        break;
                    case AcroFields.FIELD_TYPE_LIST:
                        jsonValue=" 'name': '"+key+"','type' : 'List'";
                        break;
                    case AcroFields.FIELD_TYPE_NONE:
                        jsonValue=" 'name': '"+key+"','type' : 'None'";
                        break;
                    case AcroFields.FIELD_TYPE_PUSHBUTTON:
                        jsonValue=" 'name': '"+key+"','type' : 'Pushbutton'";
                        break;
                    case AcroFields.FIELD_TYPE_RADIOBUTTON:
                        jsonValue=" 'name': '"+key+"','type' : 'Radiobutton'";
                        break;
                    case AcroFields.FIELD_TYPE_SIGNATURE:
                        jsonValue=" 'name': '"+key+"','type' : 'Signature'";
                        break;
                    case AcroFields.FIELD_TYPE_TEXT:
                        jsonValue=" 'name': '"+key+"','type' : 'Text'";
                        break;
                    default:
                        jsonValue=" 'name': '"+key+"','type' : '?'";
                        break;
                }
            //
            // Position du champ
            List<AcroFields.FieldPosition> fldPos = form.getFieldPositions(key);
            //form.setFieldCache();
            //for (AcroFields.FieldPosition pos : fldPos ) key=key+" 'position' : "+pos.toString();
            jsonValue+=" ,'position' : '"+fldPos.get(0).position.toString()+"'";

            out.write("{"+jsonValue+"}");
            out.write('\n');

        }
        out.flush();
        out.close();
        reader.close();
    }

}