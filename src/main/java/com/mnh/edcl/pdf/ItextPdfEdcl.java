package com.mnh.edcl.pdf;

import com.itextpdf.text.*;

import java.awt.*;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import com.itextpdf.text.Font;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.*;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfAction;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfDestination;
import com.itextpdf.text.pdf.PdfOutline;
import com.itextpdf.text.pdf.PdfWriter;

import java.io.ByteArrayOutputStream;
import java.io.File;

/*
        Principe  :
        1) Dans la servlet  : copier le fichier à partir d'une version template
        2) Renvoyer pdf pré-chargé (retourner mime (application/pdf)
        3) Bouton envoyer dans le document pdf : envoyer (puis ferme le doc)

 */


public class ItextPdfEdcl {



    private void createPdf() throws Exception {
        Document doc = new Document();
        PdfSmartCopy copy = new PdfSmartCopy(doc, new FileOutputStream("result.pdf"));
        doc.open();

        PdfReader mainReader = new PdfReader("timesheet.pdf");

        PdfReader reader;
        PdfStamper stamper;
        AcroFields form;
        ByteArrayOutputStream baos;

        //
        // Positionnement des Metatdatas
        //
        doc.addTitle("Sample PDF");
        doc.addSubject("Utilisant iText");
        doc.addKeywords("Java, PDF, iText");
        doc.addAuthor(System.getProperty("user.name"));
        doc.addCreator(System.getProperty("user.name"));

        for(int i = 0; i < mainReader.getNumberOfPages(); i++) {

            reader = new PdfReader(mainReader);
            baos = new ByteArrayOutputStream();
            stamper = new PdfStamper(reader, baos);
            form = stamper.getAcroFields();

            //methods to fill forms

            stamper.setFormFlattening(true);
            stamper.close();

            reader = new PdfReader(baos.toByteArray());
            copy.addPage(copy.getImportedPage(reader, 1));
        }

        doc.close();
    }

    public static void fillPdf() {
        PdfReader pdfTemplate = null;
        FileOutputStream fileOutputStream = null;
        PdfStamper stamper = null;
        PdfWriter writer;
        String RESOURCE = "C:\\Users\\Dom\\Downloads\\pdf.js";

        try {
            //Charger le modele PDF
            pdfTemplate = new PdfReader("C:\\Users\\Dom\\Downloads\\MNH-PRESTATION-AC-IT-DG20170808_V1.05.pdf");
            //
            // et supprimer les droits
            if (pdfTemplate.hasUsageRights()) {
                pdfTemplate.removeUsageRights();
            }
            fileOutputStream = new FileOutputStream("C:\\Users\\Dom\\Downloads\\MNH-PRESTATION-AC-IT-DG20170808_V1.05.v3.pdf");

            stamper = new PdfStamper(pdfTemplate, fileOutputStream);
            stamper.setFormFlattening(false);

            //
            // Charger les champs avec les valeurs
            //
            stamper.getAcroFields().setField("Nom", "PARIS");
            stamper.getAcroFields().setField("Prénom", "Dominique");

            writer = stamper.getWriter();

            //
            //  Ajout du fichier pdf.js
            //
            stamper.addJavaScript( Utilities.readFileToString(RESOURCE));

            //
            //  Création de l'action
            //
            PdfAction jsAction = PdfAction.javaScript("app.alert('Document transmis'); this.close();\r",writer);

            //
            // exemple : quand on ouvre la page 2 , l'action est déclenchée
            //
            stamper.setPageAction( PdfWriter.PAGE_OPEN, jsAction, 2);

            //
            // =====================================================================
            // Gestion du bouton envoyer vers traitement
            // =====================================================================

            PushbuttonField button = new PushbuttonField(stamper.getWriter(), new Rectangle(36, 550, 85, 570), "post");
            button.setText("ENVOYER DOCUMENT");
            button.setBackgroundColor(new GrayColor(0.7f));
            button.setVisibility(PushbuttonField.VISIBLE_BUT_DOES_NOT_PRINT);
            PdfFormField submit = button.getField();
            submit.setAdditionalActions( PdfAnnotation.AA_DOWN, jsAction );

            //submit.setAction(PdfAction.createSubmitForm("http://w10desk:8080/serv_gf_pdf_war_exploded/pdfservlet", null, PdfAction.SUBMIT_HTML_FORMAT | PdfAction.SUBMIT_COORDINATES));
            submit.setAction(PdfAction.createSubmitForm("http://w10desk:8080/serv_gf_pdf_war_exploded/pdfservlet", null, PdfAction.SUBMIT_HTML_FORMAT ));
           //submit.setAdditionalActions(stamper, PdfAction.javaScript("app.alert('action!')",writer));

            stamper.addAnnotation(submit, 1);

/*
            PushbuttonField button_2 = new PushbuttonField( stamper.getWriter(), new Rectangle(90, 50, 140, 20), "submit");
            button_2.setText("ENVOI");
            button_2.setBackgroundColor(new GrayColor(0.7f));
            button_2.setVisibility(PushbuttonField.VISIBLE_BUT_DOES_NOT_PRINT);
            PdfFormField submit_2 = button_2.getField();
            submit_2.setAction(PdfAction.createSubmitForm("http://w10desk:8080/serv_gf_pdf_war_exploded/pdfservlet", null, PdfAction.SUBMIT_EXCL_F_KEY));
            stamper.addAnnotation(submit_2, stamper.getReader().getNumberOfPages());
            stamper.addJavaScript(Utilities.readFileToString(RESOURCE));
*/


        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (DocumentException e) {
            e.printStackTrace();
        } finally {
            try {
                stamper.close();
            } catch (Exception e) {
            }
            try {
                pdfTemplate.close();
            } catch (Exception e) {
            }
            try {
                fileOutputStream.close();
            } catch (Exception e) {
            }
        }
    }

    public static void AddJavascriptToPdf() {

        // step 1: creation of a document-object 
        Document document = new Document();
        Document remote = new Document();
        try{
            // step 2: 
            PdfWriter writer = PdfWriter.getInstance(document,new FileOutputStream("C:\\Users\\Dom\\Downloads\\MNH-PRESTATION-AC-IT-DG20170808_V1.05.v3.pdf",true));
            //PdfWriter.getInstance(remote,new FileOutputStream("remote.pdf"));
            // step 3: 
            writer.setViewerPreferences(PdfWriter.PageModeUseOutlines);
            document.open();
            //remote.open();
            // step 4: 
            // we add some content 
            document.add(new Paragraph("Outline action example"));

            // we add the outline 
            PdfContentByte cb = writer.getDirectContent();
            PdfOutline root = cb.getRootOutline();
            PdfOutline links = new PdfOutline(root,new PdfAction("http://www.lowagie.com/iText/links.html"),"Useful links");
            links.setColor(new BaseColor(128, 44, 47));
            links.setStyle(Font.BOLD);
            new PdfOutline(links ,new PdfAction("http://www.lowagie.com/iText"), "Bruno's iText site");
            new PdfOutline(links ,new PdfAction("http://itextpdf.sourceforge.net/"),"Paulo's iText site");
            new PdfOutline(links ,new PdfAction("http://sourceforge.net/projects/itext/"),"iText @ SourceForge");
            PdfOutline other = new PdfOutline(root,new PdfDestination(PdfDestination.FIT),"other actions",false);
            other.setStyle(Font.ITALIC);
            new PdfOutline(other,new PdfAction("remote.pdf",1),"Go to yhe first page of a remote file");
            new PdfOutline(other,new PdfAction("remote.pdf","test"),"Go to a local destination in a remote file");
            new PdfOutline(other,PdfAction.javaScript("app.alert('Hello');\r",writer),"Say Hello");

            //remote.add(new Paragraph("Some remote document"));
            //remote.newPage();
            //Paragraph p = new Paragraph("This paragraph contains a ");
            //p.add(new Chunk("local destination").setLocalDestination("test"));
            //remote.add(p);
        }catch (DocumentException de) {
            System.err.println(de.getMessage());
        }catch (IOException ioe){
            System.err.println(ioe.getMessage());
        }
        // step 5: we close the document 
        document.close();
        //remote.close();

    }
 /*   static void ListFieldNames (string pdfSource) {

        // Création d'un objet PDF Reader basé sur le formulaire PDF
        Console.WriteLine(pdfSource + " :");
        PdfReader pdfReader = new PdfReader(pdfSource);
        AcroFields fields = pdfReader.AcroFields;

        // Boucle sur les différents champs du formulaire
        foreach (DictionaryEntry field in fields.Fields) {
            // nom du champ
            string key = field.Key.ToString();
            // type du champ (et selon le cas liste des valeurs possibles)
            string type = "";
            string data = "";
            switch (fields.GetFieldType(key)) {
                case AcroFields.FIELD_TYPE_CHECKBOX:
                    type = "CheckBox";
                    data = String.Join(", ", fields.GetAppearanceStates(key));
                    break;
                case AcroFields.FIELD_TYPE_COMBO:
                    type = "Combo";
                    data = String.Join(", ", fields.GetListOptionExport(key));
                    break;
                case AcroFields.FIELD_TYPE_LIST:
                    type = "List";
                    data = String.Join(", ", fields.GetListOptionExport(key));
                    break;
                case AcroFields.FIELD_TYPE_NONE:
                    type = "None";
                    break;
                case AcroFields.FIELD_TYPE_PUSHBUTTON:
                    type = "PushButton";
                    break;
                case AcroFields.FIELD_TYPE_RADIOBUTTON:
                    type = "RadioButton";
                    data = String.Join(", ", fields.GetAppearanceStates(key));
                    break;
                case AcroFields.FIELD_TYPE_SIGNATURE:
                    type = "Signature";
                    break;
                case AcroFields.FIELD_TYPE_TEXT:
                    type = "Text";
                    break;
            }
            Console.Write("- " + key + " : " + type);
            if (data != "") {
                Console.WriteLine(" (valeurs possibles : " + data + ")");
            } else {
                Console.WriteLine("");
            }
        }
    }*/

}
