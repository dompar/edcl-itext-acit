package com.mnh.edcl.pdf;

import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.TextField;

import java.io.IOException;
import java.util.HashMap;


public class App 
{

    /**
     * Inspects a form.
     * @param args no arguments needed
     * @throws IOException
     * @throws DocumentException
     */
    public static void main(String[] args) throws IOException, DocumentException {
    {

        System.out.println( "Tests sur les acroforms , copie de document avec chargement de champs" );
        ItextPdfEdcl TrtPdf;
        TrtPdf = new ItextPdfEdcl();
        TrtPdf.fillPdf();
        //TrtPdf.AddJavascriptToPdf();
        new InspectForm().inspectPdf("C:\\Users\\Dom\\Downloads\\MNH-PRESTATION-AC-IT-DG20170808_V1.05.pdf", "C:\\Users\\Dom\\Downloads\\fieldflags.txt");

    }

}
}
